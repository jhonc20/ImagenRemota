package com.example.salguero.imagenremota;

import android.app.Activity;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.github.snowdream.android.widget.SmartImageView;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;


public class MainActivity extends Activity {

    private Button btnImagen;
    private SmartImageView imagen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imagen = (SmartImageView)findViewById(R.id.imagen);
        btnImagen = (Button)findViewById(R.id.btnImagen);
        btnImagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                consultaImagen();
            }
        });
    }

    public void consultaImagen(){
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://192.168.1.151:8080/ImageRemota/index.php?id=1", new AsyncHttpResponseHandler() {


            @Override
            public void onStart() {
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                if(statusCode==200){
                    try {
                        JSONArray obj = new JSONArray(new String(response));
                        Log.d("Informacion", obj.toString());
                        Log.d("Nombre",obj.optJSONObject(0).getString("nombre"));
                        String urlImagen = "http://192.168.1.151:8080/ImageRemota/imagen/"+obj.optJSONObject(0).getString("imagen");
                        Rect rect = new Rect(imagen.getLeft(),imagen.getTop(),imagen.getRight(),imagen.getBottom());
                        imagen.setImageUrl(urlImagen,rect);
                        Toast.makeText(MainActivity.this,urlImagen,Toast.LENGTH_SHORT).show();
                    }catch (JSONException e){
                        Toast.makeText(MainActivity.this,e.getMessage(),Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                Toast.makeText(MainActivity.this,"Error al cargar la imagen",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onRetry(int retryNo) {
                // called when request is retried
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
